export default async ({ app, redirect, store}) => {
    if(store.state.books === null){
        await store.dispatch('fetchBooks')
    }
    if(app.$cookies.get('currentBook') === undefined){
        return redirect('/')
    }
}
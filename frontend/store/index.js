export const state = () => ({
  user: null,
  books: null,
  status: false,
})
export const getters = {
  user : state => state.user,
  books: state => state.books,
  status: state=> state.status,
}
export const mutations = {
  setUser (state, user) {
    state.user =  user
  },
  setBooks(state, books) {
    state.books = books
  }
}
export const actions = {
  async nuxtServerInit(ctx){
      const token = this.$cookies.get('auth') || null
      if (token) {
          this.$axios.setToken(token, 'Bearer')
          await ctx.dispatch('fetchUser')
      }
  },
  async fetchUser(ctx){    
    try {
      const response = await this.$axios.get('/api/user')
      ctx.commit('setUser', response.data)  
    } catch (error) {
      if(error.response.status==401){
        ctx.commit('setUser', null)  
      }
    }
  },
  async fetchBooks(ctx){  
    try {
      const response = await this.$axios.get('/api/book/all')
      ctx.commit('setBooks', response.data)  
    } catch (error) {
      if(error.response.status == 401){
        ctx.commit('setBooks', null)  
      }
    }
  },
  async login (ctx, {email, password}) {
    return this.$axios.post('/api/get_token', {email, password}).then(async response=>{
      const token = response.data
      this.$cookies.set('auth', token, {
          path: '/',
          maxAge: 60*60*24*7,
      })
    })  
  },
  async register(ctx, {email, name, password}){
    return await this.$axios.post('/api/register', {email, name, password})
  }
}
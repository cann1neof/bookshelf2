<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Eloquent\Uploadable;

class Book extends Model
{
    use Uploadable;

    protected $fillable = ['author', 'title', 'annotation', 'url_link', 'reader_link'];
}

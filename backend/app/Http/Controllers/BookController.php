<?php

namespace App\Http\Controllers;

use App\Trais\Eloquent\Uplodable;
use App\Http\Requests\StoreAttachmentRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use \App\Book;

class BookController extends Controller
{
    public function add(Request $request){
        $user = $request->user();
        if($user->tokenCan('bookshelf:all')){
            $title = $request->title;
            $author = $request->author;
            $annotation = $request->annotation;
            $book = new Book;
            $book->title =  $title;
            $book->author = $author;
            $book->annotation = $annotation;
            $book->save();

            return response(['id'=>$book->id], 200);
        }else{
            return response('NO PERMISSION', 403);
        }
    }
    public function all(Request $request){
        return Book::all();
    }
    public function delete(Request $request, $id){
        $user = $request->user();
        if($user->tokenCan('bookshelf:all')){
            $book = Book::findOrFail($id);
            $book->delete();
            return true;
        }else{
            return response('NO PERMISSION', 403);
        }
    }
    public function changeAvailabilty(Request $request, $id){
        $user = $request->user();
        if($user->tokenCan('bookshelf:all')){
            $book = Book::findOrFail($id);
            $book->availability = !$book->availability;
            $book->save();
        }else{
            return response('NO PERMISSION', 403);
        }
    }

    public function uploadImage($id, StoreAttachmentRequest $request)
    {
        $book = Book::find($id);
        $book->img_link = $book->uploadImage($request->image);
        $book->save();
        return response(200);
    }

    public function uploadReaderFile($id, StoreAttachmentRequest $request)
    {
        $book = Book::find($id);
        $book->reader_link = $book->uploadReaderFile($request->readerFile);
        $book->save();
        return response(200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Book;
use App\Trais\Eloquent\Uplodable;
use App\Http\Requests\StoreAttachmentRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UploadsController extends Controller
{ 
    // public function store(StoreAttachmentRequest $request)
    // {
    //     $book = new Book;
    //     error_log($request->input('author'));
    //     $book->title = $request->title;
    //     $book->author = $request->author;
    //     $book->annotation = $request->annotation;
    //     $book->img_link = $book->uploadImage($request->image);
    //     $book->reader_link = $book->uploadReaderFile($request->readerFile);
    //     $book->save();

    //     return response(200);
    // }

    public function createBook(Request $request)
    {
        $title = $request->title;
        $author = $request->author;
        $annotation = $request->annotation;
        $book = new Book;
        $book->title =  $title;
        $book->author = $author;
        $book->annotation = $annotation;
        $book->save();

        return response(['id'=>$book->id], 200);
    }

    public function uploadImage($id, StoreAttachmentRequest $request)
    {
        $book = Book::find($id);
        $book->img_link = $book->uploadImage($request->image);
        $book->save();
        return response(200);
    }

    public function uploadReaderFile($id, StoreAttachmentRequest $request)
    {
        $book = Book::find($id);
        $book->reader_link = $book->uploadReaderFile($request->readerFile);
        $book->save();
        return response(200);
    }
}
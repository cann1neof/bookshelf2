<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Book;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // admin 
        User::create([
            "email" => "admin@dev.dev",
            "password"=> Hash::make('dev'),
            "name"=> "Dev",
            "is_staff"=>true
        ]);

        // normal user
        User::create([
            "email" => "dev@dev.dev",
            "password"=> Hash::make('dev'),
            "name"=> "devik",
            "is_staff"=>false
        ]);

        // default book
        Book::create([
            "title" => "Война и мир",
            "author"=> "Лев Толстой",
            "annotation"=> "Великая книга великого писателя",
            "author"=> "Лев Толстой",
            "availability"=>true
        ]);
    }
}
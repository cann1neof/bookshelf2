<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
 
Route::post('/get_token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        ]);
    $user = User::where('email', $request->email)->first();
    error_log($user);
 
    if (!$user || ! Hash::check($request->password, $user->password)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }
    
    $now = (new DateTime())->format('Y-m-d H:i:s');

    if ($user->is_staff){
        $token = $user->createToken($now, ['bookshelf:all'])->plainTextToken;
    }else {
        $token = $user->createToken($now)->plainTextToken;
    }
    return $token;
});

Route::post('/register', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'name' => 'required'
    ]);
    
    $name = $request->name;
    $email = $request->email;
    $password = $request->password;
    if(User::where('email', $email)->exists()){
        // throw ValidationException::withMessages([
        //     'email' => ['The provided credentials are incorrect.'],
        // ]);
        return response('emailError', 400);
    }
    $user = User::create([
        "email" => $email,
        "password"=> Hash::make($password),
        "name"=> $name,
        "is_staff"=>false
    ]);
    
    return response('success', 201);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    error_log('kek');
    return $request->user();
});
Route::middleware('auth:sanctum')->post('/book/add', 'BookController@add');
Route::middleware('auth:sanctum')->post('/book/{id}/img', 'UploadsController@uploadImage');
Route::middleware('auth:sanctum')->post('/book/{id}/file', 'UploadsController@uploadReaderFile');

Route::get('/book/all', 'BookController@all');
Route::middleware('auth:sanctum')->post('/book/delete/{id}', 'BookController@delete');


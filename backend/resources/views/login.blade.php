<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- Подключаем Bootstrap, чтобы не работать над дизайном проекта -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    @verbatim
    <div id="app">
        <div class="container mt-5">
            <form action="/adminauth" method="POST">
                <table  style="width: 60%;" class="mx-auto"> 
                    <tr>
                        <td>
                            <input name="login" type="text" class="form-control" placeholder="Логин" v-model="login" >
                        </td>
                        <td>
                            <input name="password" type="password" class="form-control" placeholder="Пароль" v-model="password" >
                        </td>
                    </tr>
                </table>
                <div class="text-center mt-2">
                    <button type="submit" class="btn btn-outline-success">
                        Добавить
                    </button> 
                </div>
            </form>
        </div>
    </div>
    @endverbatim

    <!--Подключаем axios для выполнения запросов к api -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>

    <!--Подключаем Vue.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js"></script>

<script>
new Vue({
    el: '#app',
    data: {
        login : '',
        password : '',
    },

});
</script>
</body>
</html>